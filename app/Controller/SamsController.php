<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class SamsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Sams3NetworkSite1govnet');

/**
 * This controller uses following components
 *
 * @var array
 */
public $components = array('Paginator');

public $paginate = array('limit' => 20);


/**
 * Displays splash view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function splash() {
		$this->layout = "splash";
	}

/**
 * Displays network topology
 *
 * @return void
 */
	public function network_map() {
		$this->layout = "dashboard";
	}

/**
 * Displays dashboard frame
 *
 * @return void
 */
	public function dashboard_main() {
		$this->layout = "dashboard";
	}

/**
 * Displays dashboard frame
 *
 * @return void
 */
	public function detailed_view() {
		$this->layout = "dashboard";

		$allsite = $this->Sams3NetworkSite1govnet->find('count');
		$this->set('allsite',$allsite);

		$this->Sams3NetworkSite1govnet->recursive = 0;

		$this->set('sams3NetworkSite1govnets', $this->Paginator->paginate());
	}

/**
 * Displays fault manager frame
 *
 * @return void
 */
	public function fault_manager() {
		$this->layout = "dashboard";
	}

/**
 * Displays performance manager frame
 *
 * @return void
 */
	public function performance_manager() {
		$this->layout = "dashboard";
	}

/**
 * Displays faq management
 *
 * @return void
 */
	public function faq() {
		$this->layout = "dashboard";
	}

/**
 * Displays network report
 *
 * @return void
 */
	public function network_report() {
		$this->layout = "dashboard";
	}

}

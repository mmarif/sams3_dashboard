<?php
App::uses('AppController', 'Controller');
/**
 * Sams3NetworkSite1govnets Controller
 *
 * @property Sams3NetworkSite1govnet $Sams3NetworkSite1govnet
 * @property PaginatorComponent $Paginator
 */
class Sams3NetworkSite1govnetsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Sams3NetworkSite1govnet->recursive = 0;
		$this->set('sams3NetworkSite1govnets', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Sams3NetworkSite1govnet->exists($id)) {
			throw new NotFoundException(__('Invalid sams3 network site1govnet'));
		}
		$options = array('conditions' => array('Sams3NetworkSite1govnet.' . $this->Sams3NetworkSite1govnet->primaryKey => $id));
		$this->set('sams3NetworkSite1govnet', $this->Sams3NetworkSite1govnet->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Sams3NetworkSite1govnet->create();
			if ($this->Sams3NetworkSite1govnet->save($this->request->data)) {
				return $this->flash(__('The sams3 network site1govnet has been saved.'), array('action' => 'index'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Sams3NetworkSite1govnet->exists($id)) {
			throw new NotFoundException(__('Invalid sams3 network site1govnet'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Sams3NetworkSite1govnet->save($this->request->data)) {
				return $this->flash(__('The sams3 network site1govnet has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Sams3NetworkSite1govnet.' . $this->Sams3NetworkSite1govnet->primaryKey => $id));
			$this->request->data = $this->Sams3NetworkSite1govnet->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Sams3NetworkSite1govnet->id = $id;
		if (!$this->Sams3NetworkSite1govnet->exists()) {
			throw new NotFoundException(__('Invalid sams3 network site1govnet'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Sams3NetworkSite1govnet->delete()) {
			return $this->flash(__('The sams3 network site1govnet has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The sams3 network site1govnet could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}

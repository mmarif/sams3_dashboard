<style>

  .login-page {
    width: 800px;
    padding: 10% 0 0 0;
    margin: auto;
    max-height: 500px;
  }
  .form {
    position: relative;
    z-index: 1;
    background: #FFFFFF;
    max-width: 500px;//360px;
    margin: 0 auto 25px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
  }
  .form input {
    font-family: "Roboto", sans-serif;
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 14px;
  }
  .form button {
    font-family: "Roboto", sans-serif;
    text-transform: uppercase;
    outline: 0;
    background: #1483C5;
    width: 100%;
    border: 0;
    padding: 15px;
    color: #FFFFFF;
    font-size: 14px;
    -webkit-transition: all 0.3 ease;
    transition: all 0.3 ease;
    cursor: pointer;
  }
  .form button:hover,.form button:active,.form button:focus {
    background: #1483C5;
  }
  .form .message {
    margin: 15px 0 0;
    color: #b3b3b3;
    font-size: 12px;
  }
  .form .message a {
    color: #E53D00;
    text-decoration: none;
  }

  .container {
    position: relative;
    z-index: 1;
    max-width: 900px;
    margin: 0 auto;
  }
  .container:before, .container:after {
    content: "";
    display: block;
    clear: both;
  }
  .login-page .container .info {
    margin: 50px auto;
    text-align: center;
  }
  .container .info h1 {
    margin: 0 0 15px;
    padding: 0;
    font-size: 36px;
    font-weight: 300;
    color: #1a1a1a;
  }
  .container .info span {
    color: #4d4d4d;
    font-size: 12px;
  }
  .container .info span a {
    color: #000000;
    text-decoration: none;
  }
  .container .info span .fa {
    color: #EF3B3A;
  }

  .main-title {
    padding-bottom: 10px;
    text-align:right;
  }
  .main-title h4 {
    color:  #1483C5;
  }
  .main-title h5 {
    color:#000;
  }

  .copyright {
    text-align: right;
    color: #E21E3C;
    margin-top:40px;
  }

  .copyright.inverse {
    color: #000;
    margin-top: 0px;
  }

  .login-text {
    background-color: #D9EAFE;
    opacity: 1;
    height: 100%;
    max-height: 394px;
    max-width: 500px;
    margin: 0 auto 25px;
    padding: 30px 45px 45px 35px;  
  }

  .logo-login {
    padding-top: 30px;
  }
</style>

<div class="login-page row-fluid">

  <div class="login-text col-sm-7">
    <div class="main-title">
      <h4>1Gov*Net Service Assurance Management System</h4>
      <h5>GITN Interactive Fault & Performance Dashboard</h5>
      <img src="<?php echo SITE_URL.'img/logo/1-govnet-logo.png';?>" width="150px" class="logo-login">&nbsp;&nbsp;
      <img src="<?php echo SITE_URL.'img/logo/mampu-logo.png';?>" width="150px" class="logo-login">
    </div>
    <div class="copyright">Copyright &copy; 2013-2017 GITN Sdn Bhd</div>
    <div class="copyright inverse">Telekom Malaysia Berhad</div>
  </div>

  <div class="form col-sm-5">
    <legend><i class="fa fa-lock" aria-hidden="true"></i> Access SAMS 3.0</legend>
    <?php echo $this->element('Usermgmt.ajax_validation', array('formId' => 'loginForm', 'submitButtonId' => 'loginSubmitBtn')); ?>
		<?php echo $this->Form->create('User', array('id'=>'loginForm', 'class'=>'login-form')); ?>

      	<?php echo $this->Form->input('email', array('type'=>'text', 'label'=>false, 'div'=>false, 'placeholder'=>__('User ID'), 'class'=>'form-control')); ?>
      	<?php echo $this->Form->input('password', array('type'=>'password', 'label'=>false, 'div'=>false, 'placeholder'=>__('Password'), 'class'=>'form-control')); ?>

     	<?php echo $this->Form->Submit('Log In', array('div'=>false, 'class'=>'btn btn- login-text', 'id'=>'loginSubmitBtn')); ?>
      	<?php echo $this->Form->end(); ?>
      <?php echo $this->Html->link(__('Forgot Password?'), '/forgotPassword', array('class'=>'message')); ?>
    
  </div>

</div>

<!--div class="row">
	<div class="um-panel col-md-6 col-md-offset-3">
		<div class="um-panel-header">
			<span class="um-panel-title">
				<?php
					echo __('Sign In');
					if(SITE_REGISTRATION) {
						echo ' '.__('or');
					}?>
			</span>
			<?php if(SITE_REGISTRATION) { ?>
			<span class="um-panel-title">
				<?php echo $this->Html->link(__('Sign Up', true), '/register');?>
			</span>
			<?php } ?>
		</div>
		<div class="um-panel-content">
			<?php echo $this->element('Usermgmt.ajax_validation', array('formId' => 'loginForm', 'submitButtonId' => 'loginSubmitBtn')); ?>
			<?php echo $this->Form->create('User', array('id'=>'loginForm', 'class'=>'form-horizontal')); ?>
			<div class="um-form-row form-group">
				<label class="col-sm-4 control-label required"><?php echo __('Email / Username');?></label>
				<div class="col-sm-7">
					<?php echo $this->Form->input('email', array('type'=>'text', 'label'=>false, 'div'=>false, 'placeholder'=>__('Email / Username'), 'class'=>'form-control')); ?>
				</div>
			</div>
			<div class="um-form-row form-group">
				<label class="col-sm-4 control-label required"><?php echo __('Password');?></label>
				<div class="col-sm-7">
					<?php echo $this->Form->input('password', array('type'=>'password', 'label'=>false, 'div'=>false, 'placeholder'=>__('Password'), 'class'=>'form-control')); ?>
				</div>
			</div>
			<?php if(USE_REMEMBER_ME) { ?>
			<div class="um-form-row form-group">
			<?php   if(!isset($this->request->data['User']['remember'])) {
						$this->request->data['User']['remember']=true;
					} ?>
				<label class="col-sm-4 control-label"><?php echo __('Remember me');?></label>
				<div class="col-sm-7">
					<?php echo $this->Form->input('remember', array('type'=>'checkbox', 'label'=>false, 'div'=>false)); ?>
				</div>
			</div>
			<?php } ?>
			<?php if($this->UserAuth->canUseRecaptha('login')) { ?>
			<div class="um-form-row form-group">
				<label class="col-sm-4 control-label required"><?php echo __('Prove you\'re not a robot');?></label>
				<div class="col-sm-7">
					<?php echo $this->UserAuth->showCaptcha(isset($this->validationErrors['User']['captcha'][0]) ? $this->validationErrors['User']['captcha'][0] : ""); ?>
				</div>
			</div>
			<?php } ?>
			<div class="um-button-row">
				<?php echo $this->Form->Submit('Sign In', array('div'=>false, 'class'=>'btn btn-primary', 'id'=>'loginSubmitBtn')); ?>
				<?php echo $this->Html->link(__('Forgot Password?'), '/forgotPassword', array('class'=>'right btn btn-default')); ?>
				<?php echo $this->Html->link(__('Email Verification'), '/emailVerification', array('class'=>'right btn btn-default')); ?>
			</div>
			<?php echo $this->Form->end(); ?>
			<?php echo $this->element('Usermgmt.provider'); ?>
		</div>
	</div>
</div-->
<div id="updatePermissionIndex">
	<?php echo $this->Search->searchForm('UserSetting', array('legend' => false, 'updateDivId' => 'updatePermissionIndex')); ?>
	<?php echo $this->element('Usermgmt.paginator', array('useAjax' => true, 'updateDivId' => 'updatePermissionIndex')); ?>

	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th><?php echo __('Sr. No.');?></th>
				<th><?php echo $this->Paginator->sort('UserSetting.category', __('Category')); ?></th>
				<th><?php echo $this->Paginator->sort('UserSetting.name_public', __('Setting Name')); ?></th>
				<th><?php echo __('Setting Value');?></th>
				<th><?php echo __('Action');?></th>
			</tr>
		</thead>
		<tbody>
	<?php   if(!empty($userSettings))   {
				$page = $this->request->params['paging']['UserSetting']['page'];
				$limit = $this->request->params['paging']['UserSetting']['limit'];
				$i=($page-1) * $limit;
				foreach ($userSettings as   $row) {
					$i++;
					echo "<tr>";
					echo "<td>".$i."</td>";
					echo "<td>".ucwords(strtolower($row['UserSetting']['category']))."</td>";
					echo "<td>".h($row['UserSetting']['name_public'])."</td>";
					echo "<td>";
					if ($row['UserSetting']['type']=='input') {
						echo h($row['UserSetting']['value']);
					} elseif($row['UserSetting']['type']=='checkbox') {
						if(!empty($row['UserSetting']['value'])) {
							echo __('Yes');
						} else {
							echo __('No');
						}
					}
					echo"</td>";
					echo "<td>";
					echo "<div class='btn-group'>";
						echo "<button class='btn btn-primary dropdown-toggle' data-toggle='dropdown'>Action <span class='caret'></span></button>";
						echo "<ul class='dropdown-menu'>";
							echo "<li>".$this->Html->link(__('Edit Setting'), array('controller'=>'UserSettings', 'action'=>'editSetting', $row['UserSetting']['id'], 'page'=>$page), array('escape'=>false))."</li>";
						echo "</ul>";
					echo "</div>";
					echo "</td>";
					echo "</tr>";
				}
			} else {
				echo "<tr><td colspan=5><br/><br/>".__('No Data')."</td></tr>";
			} ?>
		</tbody>
	</table>
	<?php if(!empty($userSettings)) { echo $this->element('Usermgmt.pagination', array("totolText" => __('Number of Settings'))); } ?>
</div>
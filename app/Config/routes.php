<?php
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'sams', 'action' => 'splash'));
	Router::connect('/network', array('controller' => 'sams', 'action' => 'network_map','plugin' => ''));
	Router::connect('/service', array('controller' => 'sams', 'action' => 'dashboard_main','plugin' => ''));
	Router::connect('/fault', array('controller' => 'sams', 'action' => 'fault_manager','plugin' => ''));
	Router::connect('/performance', array('controller' => 'sams', 'action' => 'performance_manager','plugin' => ''));
	Router::connect('/faq', array('controller' => 'sams', 'action' => 'faq','plugin' => ''));
	Router::connect('/adminpanel', array('plugin' => 'usermgmt','controller' => 'users', 'action' => 'dashboard'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

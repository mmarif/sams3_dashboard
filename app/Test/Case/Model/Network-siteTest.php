<?php
App::uses('Network-site', 'Model');

/**
 * Network-site Test Case
 */
class Network-siteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'n'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Network-site = ClassRegistry::init('Network-site');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Network-site);

		parent::tearDown();
	}

}

<?php
App::uses('Sams3NetworkSite1govnet', 'Model');

/**
 * Sams3NetworkSite1govnet Test Case
 */
class Sams3NetworkSite1govnetTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sams3_network_site1govnet'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sams3NetworkSite1govnet = ClassRegistry::init('Sams3NetworkSite1govnet');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sams3NetworkSite1govnet);

		parent::tearDown();
	}

}

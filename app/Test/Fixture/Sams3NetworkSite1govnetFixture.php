<?php
/**
 * Sams3NetworkSite1govnet Fixture
 */
class Sams3NetworkSite1govnetFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'sams3_network_site_1govnet';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'SITE_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'SITE_NAME' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SITE_STATE' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SITE_REGION' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'NET_HEALTH' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'MINISTRY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'AGENCY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'MINISTRY_AGENCY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'BMT_WANIP' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'BMT_URL' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'BMT_STATUS' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'BMT_CATEGORY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SPECTRUM_SLG' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SPECTRUM_SPEED' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SE_REMARKS' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'DEVICE_STATUS' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SPECTRUM_NOTE' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'GPS_LAT' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'GPS_LONG' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'SITE_ID' => 1,
			'SITE_NAME' => 'Lorem ipsum dolor sit amet',
			'SITE_STATE' => 'Lorem ipsum dolor sit amet',
			'SITE_REGION' => 'Lorem ipsum dolor sit amet',
			'NET_HEALTH' => 'Lorem ipsum dolor sit amet',
			'MINISTRY' => 'Lorem ipsum dolor sit amet',
			'AGENCY' => 'Lorem ipsum dolor sit amet',
			'MINISTRY_AGENCY' => 'Lorem ipsum dolor sit amet',
			'BMT_WANIP' => 'Lorem ipsum dolor sit amet',
			'BMT_URL' => 'Lorem ipsum dolor sit amet',
			'BMT_STATUS' => 'Lorem ipsum dolor sit amet',
			'BMT_CATEGORY' => 'Lorem ipsum dolor sit amet',
			'SPECTRUM_SLG' => 'Lorem ipsum dolor sit amet',
			'SPECTRUM_SPEED' => 'Lorem ipsum dolor sit amet',
			'SE_REMARKS' => 'Lorem ipsum dolor sit amet',
			'DEVICE_STATUS' => 'Lorem ipsum dolor sit amet',
			'SPECTRUM_NOTE' => 'Lorem ipsum dolor sit amet',
			'GPS_LAT' => 1,
			'GPS_LONG' => 1
		),
	);

}

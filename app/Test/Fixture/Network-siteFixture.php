<?php
/**
 * Network-site Fixture
 */
class Network-siteFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'SITE_ID' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'SITE_NAME' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'MINISTRY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'MINISTRY_AGENCY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'AGENCY' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'SITE_ADDRESS' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'STATE_NAME' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'NET_SPEED' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'BMT_URL' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'NET_UTIL_IN' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,0', 'unsigned' => false),
		'NET_UTIL_OUT' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => '10,0', 'unsigned' => false),
		'NET_HEALTH' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'GPS_LONG' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'GPS_LAT' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'TECHNOLOGY' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'SITE_ID' => 1,
			'SITE_NAME' => 'Lorem ipsum dolor sit amet',
			'MINISTRY' => 'Lorem ipsum dolor sit amet',
			'MINISTRY_AGENCY' => 'Lorem ipsum dolor sit amet',
			'AGENCY' => 'Lorem ipsum dolor sit amet',
			'SITE_ADDRESS' => 'Lorem ipsum dolor sit amet',
			'STATE_NAME' => 'Lorem ipsum dolor sit amet',
			'NET_SPEED' => 'Lorem ipsum dolor sit amet',
			'BMT_URL' => 'Lorem ipsum dolor sit amet',
			'NET_UTIL_IN' => '',
			'NET_UTIL_OUT' => '',
			'NET_HEALTH' => 'Lorem ipsum dolor sit amet',
			'GPS_LONG' => 1,
			'GPS_LAT' => 1,
			'TECHNOLOGY' => 'Lorem ipsum dolor sit amet'
		),
	);

}

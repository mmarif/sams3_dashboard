<?php
App::uses('AppModel', 'Model');
/**
 * Sams3NetworkSite1govnet Model
 *
 */
class Sams3NetworkSite1govnet extends AppModel {

/**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'sams3';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'sams3_network_site_1govnet';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'SITE_ID';

}

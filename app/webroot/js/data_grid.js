(function ($) {
    var methods = {
        init: function (params) {
            var header = params.header;
            var data = params.data;
            var url = params.url;
            var input_types = params.input_types;
            var num_per_page= parseInt(params.num_per_page);
            if (isNaN(num_per_page)) {
                num_per_page = 'undefined';
            } else if (num_per_page == 0) {
                num_per_page = 'undefined'
            }
            var self = this;
            var id = self.attr("id");
            var is_table = false;


            if (typeof header == 'undefined' && typeof data == 'undefined') {
                is_table = true;
                data = methods.getTableData(id);
                header = methods.getLineNames(id);

            }
            if (is_table) {
                $(self).wrap("<div id=" + id + "></div> ");
                $(self).remove();
                self = $("#" + id);
            }

            methods.drawHeaderTable(header, data, self);
            methods.drawBodyTable(header, data, url);
            methods.drawHeaderClick(num_per_page);
            methods.filterSearch(input_types, data,num_per_page);
            methods.drawSelectOption();
            methods.drawTrUrl(data, url);
            methods.drawPagination(num_per_page);


        },
        drawHeaderTable: function (header, data, self) {
            pro_grid = $("<table border='on' id='be-userList'></table>");
            pro_grid.append("<thead id='be-headerList'><tr></tr></thead>");
            var header_html = "";
            for (var i = 0; i < header.length; i++) {
                header_html += "<th class='be-header' abbr='" + i + "'>" + header[i] + "</th>";
            }
            pro_grid.find("tr").append(header_html);
            self.empty().append(pro_grid);
        },
        drawBodyTable: function (header, data, url) {
            pro_grid.append("<tbody></tbody>");
            var body_html = "";
            var regexp=/([^"']*\.(?:png|jpg|jpeg|gif|svg))/;
            for (var i = 0; i < data.length; i++) {
                body_html += '<tr class="be_paging"';
                var y = data[i][data[i].length - 1];
                if (url != '') {
                    for (var q = 0; q < url.length; q++) {
                        if (y == url[q][0]) {
                            body_html += 'data-link="' + url[q][1] + '" title="' + url[q][1]+'"';
                        }
                    }
                }
                body_html += '>';
                for (var j = 0; j < data[i].length - 1; j++) {
                    body_html += "<td>";
                     if(regexp.test(data[i][j])==true){
                        body_html += "<div class='be-pg-image'><img src="+data[i][j]+"></div>"
                     }else{
                         body_html += data[i][j];
                     }
                    body_html += "</td>";
                }
                body_html += "</tr>";
            }
            pro_grid.children("tbody ").append(body_html);
            methods.drawOddEven();
        },
        drawTrUrl: function (data, url) {
            var tr_count = pro_grid.children("tbody ").children('tr');
            tr_count.each(function (index) {
                $(this).click(function () {
                    if ($(this).is('[data-link]')) {
                        location.href = $(this).attr('data-link');
                    }
                });

            });
        },
        drawOddEven: function () {
            pro_grid.find("tbody tr:visible:odd").css({"background-color": "#FEFEFE"});
            pro_grid.find("tbody tr:visible:even").css({"background-color": "#F1F1F1"});
        },
        drawHeaderClick: function (num_per_page) {
            pro_grid.find('.be-header').click(function () {
                var to = "";
                var this_val = this;
                if ($(this).hasClass("be-asc")) {
                    to = "be-desc";
                }
                else {
                    to = "be-asc";
                }
                pro_grid.find(".be-header").removeClass("be-asc").removeClass("be-desc");
                $(this).addClass(to);
                var rows = pro_grid.find("tr:not(:has('th'))").toArray().sort(methods.sortRows($(this).index()));
                this_val.asc = !this_val.asc;
                if (!this_val.asc) {
                    rows = rows.reverse();

                }
                for (var i = 0; i < rows.length + 1; i++) {
                    pro_grid.append(rows[i]);
                    methods.drawOddEven();
                }
                $('table').siblings('.be_pager').empty().append(methods.changePage(pro_grid.find('tbody tr.be_paging').length,num_per_page));
            });
        },
        sortRows: function (index) {
            return function (a, b) {
                var value1 = methods.getCellValue(a, index), value2 = methods.getCellValue(b, index);
                return value1 < value2 ? -1 : ((value1 > value2));
            };
        },
        getCellValue: function (row, index) {
            return $(row).children('td').eq(index).html();
        },
        filterSearch: function (input_types, data,num_per_page) {
            var min = '';
            var max = '';
            var filterRow = $('<tr id="be-input-content">').insertAfter(pro_grid.find('th:last()').parent());
            for (var j = 0; j < input_types.length; j++) {
                var arg = input_types[j];
                switch (arg) {
                    case 'search':
                        filterRow.append($('<th>').append($('<input class="be-inp-type ' + input_types[j] + '" placeholder="Fill here">').attr({type: input_types[j]})));
                        break;
                    case 'date':
                        filterRow.append($('<th>').append($('<input  class="be-inp-type ' + input_types[j] + '">').attr({type: input_types[j]})).css({"width": "5px"}));
                        break;
                    case 'date-range':
                        filterRow.append($('<th>').append($('<input  class="be-inp-type ' + input_types[j] + '"><input  class="be-inp-type ' + input_types[j] + '">').attr({type: "date"})).css({"width": "5px"}));
                        break;
                    case 'number':
                        min = parseFloat(data[0][j]);
                        max = parseFloat(data[0][j]);
                        for (var i = 0; i < data.length; i++) {
                            if (min > parseFloat(data[i][j])) {
                                min = parseFloat(data[i][j]);
                            }
                            else if (max < parseFloat(data[i][j])) {
                                max = parseFloat(data[i][j]);
                            }
                        }
                        filterRow.append($('<th class="be-number">').append($('<input min="' + min + '" max="' + max + '" class="be-inp-type ' + input_types[j] + '" placeholder="Fill here">').attr({type: input_types[j]})));
                        break;
                    case 'text':
                        filterRow.append($('<th>').append($('<input class="be-inp-type ' + input_types[j] + '" placeholder="Fill here">').attr({type: input_types[j]})));
                        break;
                    case 'datalist':
                        filterRow.append($('<th>').append($('<input placeholder="Fill here">').attr({
                            type: input_types[j],
                            'class': 'be-inp-type' + ' ' + input_types[j] + ' option',
                            'list': 'be_sel_options' + j
                        }).append('<datalist id = "be_sel_options' + j + '"></datalist>')));
                        break;
                    case 'range':
                        min = parseFloat(data[0][j]);
                        max = parseFloat(data[0][j]);
                        for (var i = 0; i < data.length; i++) {
                            if (min > parseFloat(data[i][j])) {
                                min = parseFloat(data[i][j]);
                            }
                            else if (max < parseFloat(data[i][j])) {
                                max = parseFloat(data[i][j]);
                            }
                        }
                        filterRow.append($('<th class="be-range-margin">').append($(
                            '<input type="text" id="be-amount-' + (j + 1) + '" class="be-amount-class"  readonly style="border:0;color:rgb(52, 52, 58); font-weight:bold;">' +
                            '<div id="be-slider-range' + (j + 1) + '"</div>')));

                        var minPriceInRupees = min;
                        var maxPriceInRupees = max;
                        var currentMinValue = min;
                        var currentMaxValue = max;
                        var slider_range = $('#be-slider-range' + (j + 1));
                        var id_range = $('#be-amount-' + (j + 1));
                        slider_range.slider({
                            range: true,
                            animate: true,
                            min: minPriceInRupees,
                            max: maxPriceInRupees,
                            values: [currentMinValue, currentMaxValue],
                            slide: function (event, ui) {
                                var id = ($(this).attr("id")).replace("be-slider-range", "");
                                $(this).siblings("#be-amount-" + id).val(ui.values[0] + " - " + ui.values[1]);
                                currentMinValue = ui.values[0];
                                currentMaxValue = ui.values[1];
                                var tooltip_1 = '<div class="be-termtip"><div class="be-termtip-inner">' + currentMinValue + '</div><div class="be-tooltip-arrow"></div></div>';
                                var tooltip_2 = '<div class="be-termtip"><div class="be-termtip-inner">' + currentMaxValue + '</div><div class="be-tooltip-arrow"></div></div>';
                                $(this).find('.ui-slider-handle').first().html(tooltip_1);
                                $(this).find('.ui-slider-handle').last().html(tooltip_2);
                            },
                            stop: function (event, ui) {
                                currentMinValue = ui.values[0];
                                currentMaxValue = ui.values[1];
                            },
                            change: function (event, ui) {
                                var tooltip_1 = '<div class="be-termtip"><div class="be-termtip-inner">' + currentMinValue + '</div><div class="be-tooltip-arrow"></div></div>';
                                var tooltip_2 = '<div class="be-termtip"><div class="be-termtip-inner">' + currentMaxValue + '</div><div class="be-tooltip-arrow"></div></div>';
                                $(this).find('.ui-slider-handle').first().html(tooltip_1);
                                $(this).find('.ui-slider-handle').last().html(tooltip_2);
                                methods.filter($(this));
                                $('table').siblings('.be_pager').empty().append(methods.changePage(pro_grid.find('tbody tr:visible').length,num_per_page));
                                methods.drawOddEven();
                            }
                        });
                        slider_range.children("a.ui-slider-handle").html("<span class='Slider_Value'></span>");
                        id_range.val(slider_range.slider("values", 0) +
                            " - " + slider_range.slider("values", 1));
                        break;
                    case 'image':
                        filterRow.append($('<th>').append('<div class="be-pg-image"><img src="css/img/pictures-icon.gif" ></div>'));
                        methods.headerClickOff(j)
                        break;
                    default:
                        filterRow.append($('<th>').append($('<input  class="be-inp-type ' + input_types[j] + '">').attr({type: 'text'})));
                }
                filterRow.find('input').on("input", function () {
                    pro_grid.find('tr').show();
                    methods.filter($(this));

                    $('table').siblings('.be_pager').empty().append(methods.changePage(pro_grid.find('tbody tr:visible').length,num_per_page));
                    methods.drawOddEven();
                });

            }

        },
        filter: function (item) {
            pro_grid.find('tbody tr').show();
            pro_grid.find('tbody tr').addClass('be_paging');
            var row_count = 0;
            pro_grid.find('tbody tr').each(function () {
                row_count++;
            });
            for (var i = 1; i < item.parent().parent().children('th').length + 1; i++) {
                var min = 0;
                var max = 0;
                var val = 0;
                var th = pro_grid.find('tr:nth-child(2) th:nth-child(' + i + ')').children().length;
                if (pro_grid.find('tr:nth-child(2) th:nth-child(' + i + ')').children().length > 1) {
                    min = pro_grid.find('tr:nth-child(2)>th:nth-child(' + i + ')').children().first().val();
                    max = pro_grid.find('tr:nth-child(2)>th:nth-child(' + i + ')').children().last().val();
                    for (var j = 1; j < row_count + 1; j++) {
                        val = pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').text().toLowerCase();
                        if (pro_grid.find('tr:nth-child(2)>th:nth-child(' + i + ')').children().first().hasClass("date-range")) {
                            if ((new Date(val) < new Date(min)) || (new Date(val) > new Date(max))) {
                                pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').parent().hide();
                                pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').parent().removeClass('be_paging');
                            }
                        } else {
                            if (pro_grid.find('tr:nth-child(2)>th:nth-child(' + (i) + ')').children().first().hasClass('be-amount-class')) {
                                var a = (min.split("-"));
                                var min_1 = a[0];
                                var max_1 = a[1];
                                if ((parseFloat(val) < parseFloat(min_1)) || (parseFloat(val) > parseFloat(max_1))) {
                                    pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + (i) + ')').parent().hide();
                                    pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').parent().removeClass('be_paging');
                                }
                            }
                        }
                    }
                }
                else {
                    for (var j = 1; j < row_count + 1; j++) {
                        if (pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').text().toLowerCase().indexOf(pro_grid.find('tr:nth-child(2)>th:nth-child(' + i + ')').children().last().val().toLowerCase()) == -1) {
                            pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').parent().hide();
                            pro_grid.find('tbody tr:nth-child(' + j + ')>td:nth-child(' + i + ')').parent().removeClass('be_paging');
                        }
                    }
                }
            }
            methods.drawOddEven();

        },
        drawSelectOption: function () {
            var combo_options = [];
            var tr_count = pro_grid.children('tbody').children('tr').length + 1;
            var j = 1;
            for (var i = 1; i < tr_count; i++) {
                combo_options[i] = [];
                for (var j = 1; j < tr_count; j++) {
                    combo_options[i][j] = pro_grid.children('tbody').find("tr:nth-child(" + j + ")").find("td:nth-child(" + i + ")").html();
                }
            }
            var i = 1;
            pro_grid.children('thead').children("tr:nth-child(2)").children("th").each(function () {
                var list = "";
                for (var j = 1; j < combo_options[i].length; j++) {
                    list += "<option>" + combo_options[i][j] + "</option>";
                }
                $(this).find('input').children("datalist").append(list);
                var duplicate = {};
                $('option').each(function () {
                    var txt = $(this).text();
                    if (duplicate[txt])
                        $(this).remove();
                    else
                        duplicate[txt] = true;
                });
                i++;
            });
        },
        getTableData: function (id) {
            var tr_count = $('#' + id + ' tr:has(td)').length;
            var td_count = $('#' + id + ' tr:nth-child(2) td').length;
            var coordinate = [];
            for (var i = 2; i < tr_count + 2; i++) {

                var data = [];
                for (var j = 0; j < td_count; j++) {
                    data.push(
                        $('#' + id + ' tr:nth-child(' + i + ') td').eq(j).text());
                }
                coordinate.push(data);
            }
            return coordinate;
        },
        getLineNames: function (id) {
            var names = [];
            return $('#' + id + ' tr:has(th)').map(function (i, v) {
                var $th = $('th', this);
                for (var i = 0; i < $th.length; i++) {
                    names.push($th.eq(i).text());
                }
                return names;
            }).get();
        },
        changePage: function (num_rows,num_per_page) {
            $(this).find('td:first').text(function (j, val) {
                return val;
            });

            $('#be-userList').each(function() {
                var current_page = 0;
                if(num_per_page == 'undefined'){
                    $('#be-userList').siblings('.be_pager').css("display", "none");
                    num_per_page = num_rows;
                }
                var table = $(this);
                table.bind('re_paginate', function() {
                    table.find('.be_paging').hide().slice(current_page * num_per_page, (current_page + 1) * num_per_page).show();
                    methods.drawOddEven();
                });
                table.trigger('re_paginate');
                var num_pages = Math.ceil(num_rows / num_per_page);
                var be_pager = $('#be-userList').siblings('.be_pager');

                for (var page = 0; page < num_pages; page++) {
                    $('<span class="be-page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function(event) {
                        current_page = event.data['newPage'];
                        table.trigger('re_paginate');
                        $(this).addClass('active').siblings().removeClass('active');
                }).appendTo(be_pager).addClass('clickable');
                }
                $('span.be-page-number:first').addClass('active');
            });


        },
        drawPagination:function(num_per_page){
            var html_str = '';
            html_str += '<div class="be_pager"></div>';
            $(html_str).insertAfter('#be-userList');
           methods.changePage($('tbody tr').length,num_per_page);
        },
        headerClickOff:function(header_index){
            pro_grid.find('thead tr th').each(function(){
                if($(this).index() == header_index){
                    $(this).off('click');
                    $(this).css({'cursor' :"default",'background-image': 'none'});
                }
            });
        }
    };
    $.fn.Be_ProGrid = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.tooltip');
        }
    };
})(jQuery);
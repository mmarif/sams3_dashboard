<div class="sams3NetworkSite1govnets form">
<?php echo $this->Form->create('Sams3NetworkSite1govnet'); ?>
	<fieldset>
		<legend><?php echo __('Add Sams3 Network Site1govnet'); ?></legend>
	<?php
		echo $this->Form->input('SITE_NAME');
		echo $this->Form->input('SITE_STATE');
		echo $this->Form->input('SITE_REGION');
		echo $this->Form->input('NET_HEALTH');
		echo $this->Form->input('MINISTRY');
		echo $this->Form->input('AGENCY');
		echo $this->Form->input('MINISTRY_AGENCY');
		echo $this->Form->input('BMT_WANIP');
		echo $this->Form->input('BMT_URL');
		echo $this->Form->input('BMT_STATUS');
		echo $this->Form->input('BMT_CATEGORY');
		echo $this->Form->input('SPECTRUM_SLG');
		echo $this->Form->input('SPECTRUM_SPEED');
		echo $this->Form->input('SE_REMARKS');
		echo $this->Form->input('DEVICE_STATUS');
		echo $this->Form->input('SPECTRUM_NOTE');
		echo $this->Form->input('GPS_LAT');
		echo $this->Form->input('GPS_LONG');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Sams3 Network Site1govnets'), array('action' => 'index')); ?></li>
	</ul>
</div>

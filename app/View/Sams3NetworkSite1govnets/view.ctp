<div class="sams3NetworkSite1govnets view">
<h2><?php echo __('Sams3 Network Site1govnet'); ?></h2>
	<dl>
		<dt><?php echo __('SITE ID'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SITE NAME'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_NAME']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SITE STATE'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_STATE']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SITE REGION'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_REGION']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NET HEALTH'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MINISTRY'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AGENCY'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['AGENCY']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MINISTRY AGENCY'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY_AGENCY']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BMT WANIP'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_WANIP']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BMT URL'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_URL']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BMT STATUS'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_STATUS']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BMT CATEGORY'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_CATEGORY']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SPECTRUM SLG'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SLG']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SPECTRUM SPEED'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SPEED']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SE REMARKS'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SE_REMARKS']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DEVICE STATUS'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['DEVICE_STATUS']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SPECTRUM NOTE'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_NOTE']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('GPS LAT'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['GPS_LAT']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('GPS LONG'); ?></dt>
		<dd>
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['GPS_LONG']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Sams3 Network Site1govnet'), array('action' => 'edit', $sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Sams3 Network Site1govnet'), array('action' => 'delete', $sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID']), array('confirm' => __('Are you sure you want to delete # %s?', $sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Sams3 Network Site1govnets'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sams3 Network Site1govnet'), array('action' => 'add')); ?> </li>
	</ul>
</div>

<div class="sams3NetworkSite1govnets index" style="margin-top:50px;">
	<table class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('SITE_ID'); ?></th>
			<th><?php echo $this->Paginator->sort('SITE_NAME'); ?></th>
			<th><?php echo $this->Paginator->sort('SITE_STATE'); ?></th>
			<th><?php echo $this->Paginator->sort('SITE_REGION'); ?></th>
			<th><?php echo $this->Paginator->sort('MINISTRY'); ?></th>
			<th><?php echo $this->Paginator->sort('AGENCY'); ?></th>
			<th><?php echo $this->Paginator->sort('MINISTRY_AGENCY'); ?></th>
			<th>BMT Graph</th>
			<th><?php echo $this->Paginator->sort('SPECTRUM_SLG'); ?></th>
			<th><?php echo $this->Paginator->sort('SPECTRUM_SPEED'); ?></th>
			<th><?php echo $this->Paginator->sort('SE_REMARKS'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($sams3NetworkSite1govnets as $sams3NetworkSite1govnet): ?>
	<?php 
		if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "UP"){
			echo "<tr bgcolor=\"#c1ffcb\">";
		} else if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "DOWN") {
			echo "<tr bgcolor=\"#ff7575\">";
		} else {
			echo "<tr bgcolor=\"#62c5e0\">";
		}
	?>
	
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID']); ?>&nbsp;</td>
		<td>
			<?php 
				if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "UP"){
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-green-icon.png\">";
				} else if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "DOWN") {
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-red-icon.png\">";
				} else {
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-blue-icon.png\">";
				};
			?>
			&nbsp;
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_NAME']); ?>&nbsp;
		</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_STATE']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_REGION']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['AGENCY']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY_AGENCY']); ?>&nbsp;</td>
		<td><?php echo $sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_URL'] ;?>
		</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SLG']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SPEED']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SE_REMARKS']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

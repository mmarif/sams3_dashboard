<style>
    .nav .active { background:#74BCFC; }
</style>


<?php
$contName = Inflector::camelize($this->params['controller']);
$actName = $this->params['action'];
$actionUrl = $contName.'/'.$actName;
$activeClass='active';
$inactiveClass='';
?>

<nav class="navbar navbar-fixed-top navbar-light bg-faded" style="background-color: #e3f2fd;">
    <a class="navbar-brand" href="<?php echo SITE_URL."/Sams/network_map"?>">
    <img src="<?php echo SITE_URL."/img/logo/1-govnet-logo.png"?>" width="100" height="30" alt="">
  </a>
  <!-- Navbar content -->
    <ul class="nav navbar-nav navbar-right">

        <?php

            if($this->UserAuth->isLogged()) {
                echo "<p class='navbar-text'>Logged in as ".' '.$var['User']['username']."</p>";
                echo "<li>".$this->Html->link(__('Sign Out'), array('controller'=>'Users', 'action'=>'logout', 'plugin'=>'usermgmt'))."</li>";
            } else {
                echo "<li class='".(($actionUrl=='Users/login') ? $activeClass : $inactiveClass)."'> <button>".$this->Html->link(__('Sign In'), array('controller'=>'Users', 'action'=>'login', 'plugin'=>'usermgmt'))."</button></li>";
            }
        ?>
    </ul>
    <ul class="nav navbar-nav">
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/network_map') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Network Topology'), array('controller'=>'Sams', 'action'=>'network_map','plugin' => ''))."</li>";
            ?>
        </li>
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/dashboard_main') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Overview'), array('controller'=>'Sams', 'action'=>'dashboard_main','plugin' => ''))."</li>";
            ?>
        </li>
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/detailed_view') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Details View'), array('controller'=>'Sams', 'action'=>'detailed_view','plugin' => ''))."</li>";
            ?>
        </li>
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/network_report') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Reports'), array('controller'=>'Sams', 'action'=>'network_report','plugin' => ''))."</li>";
            ?>
        </li>
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/fault_manager') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Fault Manager'), array('controller'=>'Sams', 'action'=>'fault_manager','plugin' => ''))."</li>";
            ?>
        </li>
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/performance_manager') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Performance Manager'), array('controller'=>'Sams', 'action'=>'performance_manager','plugin' => ''))."</li>";
            ?>
        </li>
        <li>
            <?php
                echo "<li class='nav-item ".(($actionUrl=='Sams/faq') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('FAQ'), array('controller'=>'Sams', 'action'=>'faq','plugin' => ''))."</li>";
            ?>
        </li>
    </ul>
</nav>
<!--nav id="header" class="navbar navbar-fixed-top">
    <div id="header-container" class="container navbar-container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">1GOV*NET </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">

                <?php

                    if($this->UserAuth->isLogged()) {
                        echo "<li> <a>Logged in as ".' '.$var['User']['username']."</a></li>";
                        echo "<li>".$this->Html->link(__('Sign Out'), array('controller'=>'Users', 'action'=>'logout', 'plugin'=>'usermgmt'))."</li>";
                    } else {
                        echo "<li class='".(($actionUrl=='Users/login') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Sign In'), array('controller'=>'Users', 'action'=>'login', 'plugin'=>'usermgmt'))."</li>";
                    }
                ?>
            </ul>
            <ul class="nav navbar-nav">
                <li>
                    <?php
                        echo "<li class='".(($actionUrl=='Sams/network_map') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Network Topology'), array('controller'=>'Sams', 'action'=>'network_map','plugin' => ''))."</li>";
                    ?>
                </li>
                <li>
                    <?php
                        echo "<li class='".(($actionUrl=='Sams/dashboard_main') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Network Analysis'), array('controller'=>'Sams', 'action'=>'dashboard_main','plugin' => ''))."</li>";
                    ?>
                </li>
                <li>
                    <?php
                        echo "<li class='".(($actionUrl=='Sams/network_report') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Reports'), array('controller'=>'Sams', 'action'=>'network_report','plugin' => ''))."</li>";
                    ?>
                </li>
                <li>
                    <?php
                        echo "<li class='".(($actionUrl=='Sams/fault_manager') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Fault Manager'), array('controller'=>'Sams', 'action'=>'fault_manager','plugin' => ''))."</li>";
                    ?>
                </li>
                <li>
                    <?php
                        echo "<li class='".(($actionUrl=='Sams/performance_manager') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('Performance Manager'), array('controller'=>'Sams', 'action'=>'performance_manager','plugin' => ''))."</li>";
                    ?>
                </li>
                <li>
                    <?php
                        echo "<li class='".(($actionUrl=='Sams/faq') ? $activeClass : $inactiveClass)."'>".$this->Html->link(__('FAQ'), array('controller'=>'Sams', 'action'=>'faq','plugin' => ''))."</li>";
                    ?>
                </li>
            </ul>
        </div>
    </div>
</div-->

<!--script>
    $(document).ready(function(){

    /**
     * This object controls the nav bar. Implement the add and remove
     * action over the elements of the nav bar that we want to change.
     *
     * @type {{flagAdd: boolean, elements: string[], add: Function, remove: Function}}
     */
    var myNavBar = {

        flagAdd: true,

        elements: [],

        init: function (elements) {
            this.elements = elements;
        },

        add : function() {
            if(this.flagAdd) {
                for(var i=0; i < this.elements.length; i++) {
                    document.getElementById(this.elements[i]).className += " fixed-theme";
                }
                this.flagAdd = false;
            }
        },

        remove: function() {
            for(var i=0; i < this.elements.length; i++) {
                document.getElementById(this.elements[i]).className =
                        document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
            }
            this.flagAdd = true;
        }

    };

    /**
     * Init the object. Pass the object the array of elements
     * that we want to change when the scroll goes down
     */
    myNavBar.init(  [
        "header",
        "header-container",
        "brand"
    ]);

    /**
     * Function that manage the direction
     * of the scroll
     */
    function offSetManager(){

        var yOffset = 0;
        var currYOffSet = window.pageYOffset;

        if(yOffset < currYOffSet) {
            myNavBar.add();
        }
        else if(currYOffSet == yOffset){
            myNavBar.remove();
        }

    }

    /**
     * bind to the document scroll detection
     */
    window.onscroll = function(e) {
        offSetManager();
    }

    /**
     * We have to do a first detectation of offset because the page
     * could be load with scroll down set.
     */
    offSetManager();
    });
</script-->
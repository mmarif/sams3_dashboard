<style>
  .hr{margin:5px 0;}
  .accordion-group{margin-bottom:10px;border-radius:0;}
  .accordion-toggle{
      background:rgb(248, 251, 252);
      
  }

  .accordion-toggle:hover{
    text-decoration: none;
    
  }

  .accordion-heading .accordion-toggle {
      display: block;
      padding: 8px 15px;
  }



  .selectStyle{
    width:46%; float: left; margin-right: 8%;
  }


  .accordion-group{
    margin-bottom:20px;
  }

  #faqframe{
    margin-top:80px;
  }
</style>

<div id="faqframe" class="container">
  <div class="row">
    <div class="col-lg-12">
      <h2>Frequently Asked Questions</h2><hr/>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle"  data-toggle="collapse" data-parent="toggle" href="#collapseOne">
            What is Service Assurance Management System (SAMS 3.0)?
          </a>
        </div>
      
        <div id="collapseOne" class="accordion-body collapse ">
          <div class="accordion-inner">
              <p>SAMS is a Service Assurance system that enable agency to monitor and manage network fault, performance, utilization and trouble ticket (TR) status.</p> 
              <p>The system shows dashboard Indicator with summary status of site Up, Down or Preventive Maintenance, number of sites exceed utilization threshold and number of open trouble ticket status. </p>
              <p>There are 3 types of dashboard view:</p>
              <ol>
                <li>Overall 1Gov*net summary view of all ministry and agency</li>
                <li>Ministry view of all relevant agencies
                <li>Agency view details list of sites belongs to the agency. User is able to view details status and report by Click on Fault Manager for Alarm Monitoring, Performance Manager for nework health and utilization and Report for Schedule Monthly Report. Detail application and traffic analysis report can be viewed from Agency dashboard with link to Exinda BMT dashboard.</li>
              </ol>
          </div>
        </div>
      </div>

<div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseTwo">
            How to login?
          </a>
        </div>
        <div id="collapseTwo" class="accordion-body collapse ">
          <div class="accordion-inner">
           To login: at the browser type http://www.samsv2. gitn.com.my, each agency will be provided with 2 userid and default password to login. User id and password can be changed based on request. Each agency is restricted to view only network status and list of sites belongs to that particular agency only.
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseThree">
            What is the basic system requirements?
          </a>
        </div>
        <div id="collapseThree" class="accordion-body collapse ">
          <div class="accordion-inner">
              <p>
                To access the system user need to install latest Internet browser update with Java Runtime Environment (JRE)
              </p>
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseFour">
            How to get the best display on your monitor?
          </a>
        </div>
        <div id="collapseFour" class="accordion-body collapse ">
          <div class="accordion-inner">
            The recommended browsers are Google Chrome, Firefox and Internet Explorer.
          </div>
        </div>
      </div>

 <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseFive">
            How to clear browser cache?
          </a>
        </div>
        <div id="collapseFive" class="accordion-body collapse ">
          <div class="accordion-inner">
            <p>Chrome</p>
            <p>1.  On your browser toolbar, tap More.</p>
            <p>2.  Tap History, and then tap Clear browsing data.</p>
            <p>3.  Under "Clear browsing data," select the checkboxes for Cookies and site data and Cached images and files.</p>
            <p>4.  Use the menu at the top to select the amount of data that you want to delete.</p>
            <p>5.  Tap Clear browsing data.</p>

            <p>Internet Explorer</p>
            <p>1.  Select Tools > Internet Options.</p>
            <p>2.  Click on the General tab and then the Delete button.</p>
            <p>3.  Make sure to uncheck Preserve Favorites website data and check both Temporary Internet Files and Cookies then click Delete.</p>

            <p>Mozilla Firefox</p>
            <p>1.  From the History menu, select Clear Recent History.</p>
            <p>2.  From the Time range to clear: drop-down menu, select the desired range; to clear your entire cache, select Everything.</p>
            <p>3.  Next to "Details", click the down arrow to choose which elements of the history to clear; to clear your entire cache, select all items.</p>
          </div>
        </div>
      </div>

      <div class="accordion-group">
        <div class="accordion-heading">
          <a class="accordion-toggle" data-toggle="collapse" data-parent="toggle" href="#collapseSix">
            How to disable pop up blocker?
          </a>
        </div>
        <div id="collapseSix" class="accordion-body collapse ">
          <div class="accordion-inner">
          <p>Chrome</p>
          <p>1.  At the top right, click More.</p>
          <p>2. Click Settings.</p>
          <p>3. At the bottom, click Show advanced settings.</p>
          <p>4. Under "Privacy," click Content settings.</p>
          <p>5. Under "Pop-ups," select an option:</p>
          <p>I.  Do not allow any site to show pop-ups (recommended)</p>
          <p>II. Allow all sites to show pop-ups</p>

          <p>Internet Explorer</p>
          <p>1.  Select the Tools button , and then select Internet options.</p>
          <p>2.  On the Privacy tab, under Pop-up Blocker, select or clear the Turn on Pop-up Blocker check box, and then select OK.</p>

          <p>Mozilla Firefox</p>
          <p>1.  From the Tools menu, select Options.</p>
          <p>2.  From the Content tab, uncheck Block Popup Windows and click "OK".</p>
          </div>
        </div>
      </div>


        </div>
  </div>
</div>

<script>
  // accordion pluse minus goes here        
   $('.accordion-toggle').click(function(){
      
      var has = $(this);
      if(has.hasClass('collapsed')){
         $(this).find('i').removeClass('fa-plus');
         $(this).find('i').addClass('fa-minus');
      }
      else{
        $(this).find('i').removeClass('fa-minus');
        $(this).find('i').addClass('fa-plus');
      }
  })
</script>
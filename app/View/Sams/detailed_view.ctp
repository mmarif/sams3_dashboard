<?php #debug($allsite); die(); ?>
<!-- Dummy form -->
<nav class="bg-faded" style="background-color: #07192b;">

<div class="data-grid" style="margin-top: 50px;"></div>

<script>
    $(document).ready(function () {
        var header = [
"SITE_ID",
"SITE_NAME",
"SITE_STATE",
"SITE_REGION",
"NET_HEALTH",
"NET_UTIL_IN",
"NET_UTIL_OUT",
"MINISTRY",
"AGENCY",
"MINISTRY_AGENCY",
"BMT_WANIP",
"BMT_URL",
"BMT_STATUS",
"BMT_CATEGORY",
"SPECTRUM_SLG",
"SPECTRUM_SPEED"
];
        
var input_types = [
"search", 
"search", 
"datalist", 
"datalist", 
"datalist", 
"range",
"range",
"datalist",
"datalist",
"datalist",
"search",
"search",
"datalist",
"datalist",
"datalist",
"datalist"];
        var data = [
            ["400024","Jabatan Akauntan Negara Cawangan Pulau Pinang","PPG","Northern","UP",46.24,19.71,"MOF","JANM","MOF-JANM","10.60.1.11","https://10.60.1.11/admin/launch?script=rh&template=mon-apps-tabbed&tabsel=individual&resolution_config=last_24_hours","In Use","Bandwidth Manager","99.9%","20 Mbps"],
["400036","Jabatan Akauntan Negara Cawangan Negeri Sembilan","NS","Southern","UP",95.88,21.25,"MOF","JANM","MOF-JANM","10.60.2.11","https://10.60.2.11/admin/launch?script=rh&template=mon-apps-tabbed&tabsel=individual&resolution_config=last_24_hours","In Use","Bandwidth Manager","99.9%","20 Mbps"],
["400044","Jabatan Penilaian Dan Perkhidmatan Harta Kbharu (Kota Bharu) (Konso)","KEL","East Coast","UP",57.44,25.25,"MOF","JPPH","MOF-JPPH","10.118.68.10","https://10.118.68.10/admin/launch?script=rh&template=mon-apps-tabbed&tabsel=individual&resolution_config=last_24_hours","In Use","Bandwidth Manager","99.3%","6 Mbps"],
["401276","Lembaga Kemajuan Wilayah Kedah","KDH","Northern","UP",98.28,25.9,"KKLW","KEDA","KKLW-KEDA","","-","","","99.9%","10 Mbps"],
["401385","Hospital Melaka","MLK","Southern","UP",99.13,80.77,"MOH","MELAKA","MOH-MELAKA","","-","","","99.9%","20 Mbps"],
["401411","Penjara Baru Sungai Udang","MLK","Southern","UP",98.9,20.35,"KDN","PENJARA","KDN-PENJARA","","-","","","99.9%","6 Mbps"],
["402283","Majlis Keselamatan Negara Wilayah Persekutuan KL","KL","Central","UP",98.91,7.63,"JPM","MKN","JPM-MKN","","-","","","99.7%","2 Mbps"],
["402284","Pusat Pelancongan Malaysia (MATIC)","KL","Central","UP",79.14,8.49,"MOTAC","MATIC","MOTAC-MATIC","","-","","","99.7%","20 Mbps"],
["402336","Jabatan Tenaga Kerja Serian","SWK","Sbh/Swk","UP",97.28,30.06,"MOHR","JTK","MOHR-JTK","","-","","","99.7%","2 Mbps"],
["402338","Pejabat Tenaga Kerja Bahagian Kunak","SBH","Sbh/Swk","UP",97.42,13.74,"MOHR","JTK","MOHR-JTK","","-","","","99.7%","2 Mbps"],
["402344","Jabatan Tenaga Kerja Sarawak (BSI) (Konso)","SWK","Sbh/Swk","UP",98.97,49.27,"MOHR","JTK","MOHR-JTK","","-","","","99.3%","8 Mbps"],
["402345","Jabatan Tenaga Kerja Kuching","SWK","Sbh/Swk","UP",96.86,90.99,"MOHR","JTK","MOHR-JTK","","-","","","99.7%","2 Mbps"],
["402374","Jabatan Tenaga Kerja Pahang (JTK) (Kuantan) (Konso)","PHG","East Coast","UP",71.37,15.21,"MOHR","JTK","MOHR-JTK","","-","","","99.3%","6 Mbps"],
        ];
        var url = [
            ['1', 'https://www.google.ru/'],
            ['2', 'https://www.facebook.com/'],
            ['3', 'https://vk.com/'],
            ['17', 'http://www.w3schools.com/']
        ];

        var num_per_page = '5';

        var params = {
            "header": header,
            "input_types": input_types,
            "data": data,
            'url': url,
            'num_per_page':num_per_page
        };
        
        $(".data-grid").Be_ProGrid(params);
    });
</script>

<!--div id="updateSiteIndex" style="/*margin-top:50px;*/">
	<table class="table table-bordered table-hover table-fixed">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('SITE_ID'); ?></th>
			<th><?php echo $this->Paginator->sort('SITE_NAME'); ?></th>
			<th><?php echo $this->Paginator->sort('SITE_STATE'); ?></th>
			<th><?php echo $this->Paginator->sort('SITE_REGION'); ?></th>
			<th><?php echo $this->Paginator->sort('MINISTRY'); ?></th>
			<th><?php echo $this->Paginator->sort('AGENCY'); ?></th>
			<th><?php echo $this->Paginator->sort('MINISTRY_AGENCY'); ?></th>
			<th>BMT Graph</th>
			<th><?php echo $this->Paginator->sort('SPECTRUM_SLG'); ?></th>
			<th><?php echo $this->Paginator->sort('SPECTRUM_SPEED'); ?></th>
			<th><?php echo $this->Paginator->sort('SE_REMARKS'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($sams3NetworkSite1govnets as $sams3NetworkSite1govnet): ?>
	<?php 
		if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "UP"){
			echo "<tr bgcolor=\"#c1ffcb\">";
		} else if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "DOWN") {
			echo "<tr bgcolor=\"#ff7575\">";
		} else {
			echo "<tr bgcolor=\"#62c5e0\">";
		}
	?>
	
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID']); ?>&nbsp;</td>
		<td>
			<?php 
				if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "UP"){
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-green-icon.png\">";
				} else if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "DOWN") {
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-red-icon.png\">";
				} else {
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-blue-icon.png\">";
				};
			?>
			&nbsp;
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_NAME']); ?>&nbsp;
		</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_STATE']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_REGION']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['AGENCY']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY_AGENCY']); ?>&nbsp;</td>
		<td style="text-align:center">
		 <a href="<?php echo  $sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_URL'] ;?>" target="_blank">
    		<img src="<?php echo SITE_URL."/img/link.png"?>" >
  		</a>
		</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SLG']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SPEED']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SE_REMARKS']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	
	</p>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div-->

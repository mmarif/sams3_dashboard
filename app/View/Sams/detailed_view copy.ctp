<?php #debug($allsite); die(); ?>
<!-- Dummy form -->
<nav class="bg-faded" style="background-color: #07192b;">

<script>
    $(document).ready(function () {
        var input_types = ["datalist", "datalist", "datalist", "datalist", "datalist", "datalist", "datalist", "image", "datalist","datalist","range","range"];

        var params = {
            "input_types": input_types
        };

        $("#data-grid").Be_ProGrid(params);
    });
</script>

<div id="updateSiteIndex" style="margin-top:50px;">
	<table id="data_grid" class="table table-bordered table-hover table-fixed">
	<thead>
	<tr>
			<th>SITE_ID</th>
			<th>SITE_NAME</th>
			<th>SITE_STATE</th>
			<th>SITE_REGION</th>
			<th>MINISTRY</th>
			<th>AGENCY</th>
			<th>MINISTRY_AGENCY</th>
			<th>BMT Graph</th><!-- 8-->
			<th>SPECTRUM_SLG</th>
			<th>SPECTRUM_SPEED</th>
			<th>NET UTIL IN</th>
			<th>NET UTIL OUT</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($sams3NetworkSite1govnets as $sams3NetworkSite1govnet): ?>
	<?php 
		if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "UP"){
			echo "<tr bgcolor=\"#c1ffcb\">";
		} else if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "DOWN") {
			echo "<tr bgcolor=\"#ff7575\">";
		} else {
			echo "<tr bgcolor=\"#62c5e0\">";
		}
	?>
	
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_ID']); ?>&nbsp;</td>
		<td>
			<?php 
				if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "UP"){
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-green-icon.png\">";
				} else if($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_HEALTH'] == "DOWN") {
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-red-icon.png\">";
				} else {
					echo "<img src=\"http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/24/Flag-blue-icon.png\">";
				};
			?>
			&nbsp;
			<?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_NAME']); ?>&nbsp;
		</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_STATE']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SITE_REGION']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['AGENCY']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['MINISTRY_AGENCY']); ?>&nbsp;</td>
		<td style="text-align:center">
		 <a href="<?php echo  $sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['BMT_URL'] ;?>" target="_blank">
    		<img src="<?php echo SITE_URL."/img/link.png"?>" >
  		</a>
		</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SLG']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['SPECTRUM_SPEED']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_UTIL_IN']); ?>&nbsp;</td>
		<td><?php echo h($sams3NetworkSite1govnet['Sams3NetworkSite1govnet']['NET_UTIL_OUT']); ?>&nbsp;</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	
	</p>
</div>

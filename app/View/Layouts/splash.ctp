<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>SAMS 3.0</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script language="javascript">
		var urlForJs="<?php echo SITE_URL ?>";
	</script>

	<?php
		echo $this->Html->meta('icon');
		/* Bootstrap CSS */
		echo $this->Html->css('bootstrap.css?q='.QRDN);

		echo $this->Html->css('leaflet.css?q='.QRDN);

		/* Jquery latest version taken from http://jquery.com */
		echo $this->Html->script('jquery-1.11.3.js');
		
		/* Bootstrap JS */
		echo $this->Html->script('bootstrap.js?q='.QRDN);

		echo $this->Html->script('leaflet.js?q='.QRDN);

		/* Usermgmt Plugin JS */
		echo $this->Html->script('/usermgmt/js/umscript.js?q='.QRDN);
		echo $this->Html->script('/usermgmt/js/ajaxValidation.js?q='.QRDN);

		#echo $this->Html->script('/usermgmt/js/chosen/chosen.ajaxaddition.jquery.js?q='.QRDN);

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

	?>
	<style>
        body {
            padding: 0;
            margin: 0;
        }
        html, body, #map {
            position: absolute;
			top: 0;
			bottom: 0;
			width: 100%;
			z-index: 1;
        }

        .title{
        	top:100px;
        	left:50px;
        	right:50px;
	    	position: absolute;
			overflow: auto;
			z-index: 2;
			text-align: center;
			margin-top: 10%;
			padding: -5px;
			text-shadow: 1px 1px #f8f8f8;
			color: #FFF;
	    }

	    .title a{
	    	color:#1860AA;
	    	text-decoration: none;
	    }
    </style>

</head>
<body>
	<div id="map"></div>

	<div class="title">
		<center>
		  <a href="<?php echo SITE_URL.'login';?>">
		  	<img src="<?php echo SITE_URL.'/img/logo/1-govnet-logo.png';?>" />
		  	<h1>Service Assurance Management</h1>
		  </a>
		</center>
	</div>

    <script>
        // Setup map
		var map = L.map('map').setView([4.052982, 108.292368], 7);

		// Setup tilelayer
		L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Initiative by GITN Sdn Bhd .',
            maxZoom: 18,
		  	detectRetina: true
		}).addTo(map);

		// Deploy network sites

    </script>
	<?php if(class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) { echo $this->Js->writeBuffer(); } ?>
</body>
</html>

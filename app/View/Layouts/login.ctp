<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>SAMS 3.0</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script language="javascript">
		var urlForJs="<?php echo SITE_URL ?>";
	</script>
	<?php
		echo $this->Html->meta('icon');
		/* Bootstrap CSS */
		echo $this->Html->css('bootstrap.css?q='.QRDN);
		echo $this->Html->css('../font-awesome/css/font-awesome.min.css?q='.QRDN);

		
		/* Usermgmt Plugin CSS */
		echo $this->Html->css('/usermgmt/css/umstyle.css?q='.QRDN);
		
		/* Bootstrap Datepicker is taken from https://github.com/eternicode/bootstrap-datepicker */
		/*echo $this->Html->css('/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css?q='.QRDN);*/

		/* Bootstrap Datepicker is taken from https://github.com/smalot/bootstrap-datetimepicker */
		echo $this->Html->css('bootstrap-datetimepicker.css?q='.QRDN);
		
		/* Chosen is taken from https://github.com/harvesthq/chosen/releases/ */
		/*echo $this->Html->css('/plugins/chosen/chosen.min.css?q='.QRDN);*/

		/* Jquery latest version taken from http://jquery.com */
		echo $this->Html->script('jquery-1.11.3.js');
		
		/* Bootstrap JS */
		echo $this->Html->script('bootstrap.js?q='.QRDN);

		/* Bootstrap Datepicker is taken from https://github.com/eternicode/bootstrap-datepicker */
		/*echo $this->Html->script('/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js?q='.QRDN);*/

		/* Bootstrap Datepicker is taken from https://github.com/smalot/bootstrap-datetimepicker */
		/*echo $this->Html->script('bootstrap-datetimepicker.js?q='.QRDN);*/
		
		/* Bootstrap Typeahead is taken from https://github.com/biggora/bootstrap-ajax-typeahead */
		echo $this->Html->script('typeahead.jquery.min.js?q='.QRDN);
		
		/* Chosen is taken from https://github.com/harvesthq/chosen/releases/ */
		/*echo $this->Html->script('/plugins/chosen/chosen.jquery.min.js?q='.QRDN);*/

		/* Usermgmt Plugin JS */
		echo $this->Html->script('/usermgmt/js/umscript.js?q='.QRDN);
		echo $this->Html->script('/usermgmt/js/ajaxValidation.js?q='.QRDN);

		echo $this->Html->script('/usermgmt/js/chosen/chosen.ajaxaddition.jquery.js?q='.QRDN);

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

	?>
<style>
body { 
  background: url(<?php echo SITE_URL.'img/commnunication-2.jpg';?>) no-repeat center center fixed !important; 
  -webkit-background-size: cover !important;
  -moz-background-size: cover !important;
  -o-background-size: cover !important;
  background-size: cover !important;
}
</style>
</head>
<body>
	<div class="container">
		<div class="row">
        	<div class="col-md-12">
				<div class="content">
					<?php
						if($this->UserAuth->isLogged()) {
							#echo $this->element('Usermgmt.dashboard');
							echo $this->element('menu_dashboard');
						} ?>
					<?php echo $this->element('Usermgmt.message'); ?>
					<?php echo $this->element('Usermgmt.message_validation'); ?>
					<?php echo $this->fetch('content'); ?>
					<div style="clear:both"></div>
				</div>
			</div>
		</div>
	</div>
	<?php if(class_exists('JsHelper') && method_exists($this->Js, 'writeBuffer')) { echo $this->Js->writeBuffer(); } ?>
</body>
</html>